reco4j-neo4j
============

Reco4j implementation that uses neo4j as graph database

Get started
-----------
See the following link for a get started guide: http://www.reco4j.org/get-started.jsp

Release Notes
-------------
See the following link for release notes: http://www.reco4j.org/release-notes.jsp