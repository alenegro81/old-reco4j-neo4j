/*
 * Neo4jGraphTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4jGraphTest extends TestCase
{

  private static Neo4jGraph learningDataSet;

  public Neo4jGraphTest(String testName)
  {
    super(testName);
  }

  public static Test suite()
  {
    setUpClass();
    return new TestSuite(Neo4jGraphTest.class);
  }

  public static void setUpClass()
  {
//    System.out.println("Setup test... ");
//    Properties properties = RecommenderNeo4jEngine.loadProperties();
//    learningDataSet = new Neo4jGraph();
//    learningDataSet.setProperties(properties);
//    learningDataSet.initDatabase();
//    RecommenderSessionManager.getInstance().setLearningDataSet(learningDataSet);
//    //learningDataSet.optimizeGraph();
//    System.out.println("... test setup ended!");
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

  }

  @Override
  protected void tearDown() throws Exception
  {
    super.tearDown();
  }

  /**
   * Test of getNodesByType method, of class Neo4jGraph.
   */ 
  public void testGetNodesByType()
  {


//    System.out.println("getNodesByType");
//    //Test Loading item
//    long startTime = System.currentTimeMillis();
//    List resultItem = learningDataSet.getNodesByType(Neo4JPropertiesHandle.getInstance().getItemType());
//    long endTime = System.currentTimeMillis();
//    System.out.println("Time Elapsed: " + (endTime - startTime));
//    assertEquals(1682, resultItem.size());
//    //Test Loading user
//    startTime = System.currentTimeMillis();
//    List resultUser = learningDataSet.getNodesByType(Neo4JPropertiesHandle.getInstance().getUserType());
//    endTime = System.currentTimeMillis();
//    System.out.println("Time Elapsed: " + (endTime - startTime));    
//    assertEquals(943, resultUser.size());
  }

  /**
   * Test of getNodesMapByType method, of class Neo4jGraph.
   */
  public void testGetNodesMapByType()
  {
//    System.out.println("getNodesMapByType");
//    long startTime = System.currentTimeMillis();
//    HashMap<String, INode> resultItem = learningDataSet.getNodesMapByType(
//                                              Neo4JPropertiesHandle.getInstance().getItemType(),
//                                              Neo4JPropertiesHandle.getInstance().getItemIdentifierName());
//    long endTime = System.currentTimeMillis();
//    System.out.println("Time Elapsed: " + (endTime - startTime));
//    assertEquals(1682, resultItem.values().size());
//    startTime = System.currentTimeMillis();
//    HashMap<String, INode> resultUser = learningDataSet.getNodesMapByType(
//                                              Neo4JPropertiesHandle.getInstance().getUserType(),
//                                              Neo4JPropertiesHandle.getInstance().getUserIdentifierName());
//    endTime = System.currentTimeMillis();
//    System.out.println("Time Elapsed: " + (endTime - startTime));
//    assertEquals(943, resultUser.values().size());
    
  }
}
