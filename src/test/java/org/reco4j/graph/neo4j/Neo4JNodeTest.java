/*
 * Neo4JNodeTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JNodeTest extends TestCase
{
  private static Neo4jGraph learningDataSet;

  
  public Neo4JNodeTest(String testName)
  {
    super(testName);
  }
  
  @Override
  protected void setUp() throws Exception
  {
    super.setUp();
  }
  
  @Override
  protected void tearDown() throws Exception
  {
    super.tearDown();
  }
  
  public static Test suite()
  {
    setUpClass();
    return new TestSuite(Neo4JNodeTest.class);
  }

  public static void setUpClass()
  {
    System.out.println("Setup test... ");
//    Properties properties = RecommenderNeo4jEngine.loadProperties();
//    learningDataSet = new Neo4jGraph();
//    learningDataSet.setProperties(properties);
//    learningDataSet.initDatabase();
//    RecommenderSessionManager.getInstance().setLearningDataSet(learningDataSet);
    System.out.println("... test setup ended!");
  }

  /**
   * Test of isConnected method, of class Neo4JNode.
   */
  public void testIsConnected()
  {
    System.out.println("isConnected");
    /*List<INode> users = learningDataSet.getNodesByType(Neo4JPropertiesHandle.getInstance().getUserType());
    List<INode> items = learningDataSet.getNodesByType(Neo4JPropertiesHandle.getInstance().getItemType());
    Long startTime = System.currentTimeMillis();
    int hit = 0;
    int processed = 0;
    for (INode user : users)
      for (INode item : items)
      {
        boolean res = user.isConnected(item, EdgeTypeFactory.getEdgeType(IEdgeType.EDGE_TYPE_RANK));
        if (res)
          hit++;
        processed++;
        
      }
    Long endTime = System.currentTimeMillis();
    System.out.println("Elapsed time: " + (endTime - startTime) + " Processed: " + processed + " Succeded: " + hit);*/
    assertTrue(true);
  }

//  /**
//   * Test of getEdge method, of class Neo4JNode.
//   */
//  public void testGetEdge()
//  {
//    System.out.println("getEdge");
//    INode node = null;
//    IEdgeType edgeType = null;
//    Neo4JNode instance = null;
//    IEdge expResult = null;
//    IEdge result = instance.getEdge(node, edgeType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getInEdge method, of class Neo4JNode.
//   */
//  public void testGetInEdge()
//  {
//    System.out.println("getInEdge");
//    IEdgeType edgeType = null;
//    Neo4JNode instance = null;
//    List expResult = null;
//    List result = instance.getInEdge(edgeType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getOutEdge method, of class Neo4JNode.
//   */
//  public void testGetOutEdge()
//  {
//    System.out.println("getOutEdge");
//    IEdgeType edgeType = null;
//    Neo4JNode instance = null;
//    List expResult = null;
//    List result = instance.getOutEdge(edgeType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getCommonNodes method, of class Neo4JNode.
//   */
//  public void testGetCommonNodes()
//  {
//    System.out.println("getCommonNodes");
//    IEdgeType edgeType = null;
//    Neo4JNode instance = null;
//    List expResult = null;
//    List result = instance.getCommonNodes(edgeType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of getInEdgeNumber method, of class Neo4JNode.
//   */
//  public void testGetInEdgeNumber()
//  {
//    System.out.println("getInEdgeNumber");
//    IEdgeType edgeType = null;
//    Neo4JNode instance = null;
//    int expResult = 0;
//    int result = instance.getInEdgeNumber(edgeType);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
}
