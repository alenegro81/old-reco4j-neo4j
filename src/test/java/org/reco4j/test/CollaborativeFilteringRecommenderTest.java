/*
 * CollaborativeFilteringRecommenderTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.test;

import java.util.Properties;
import junit.framework.TestCase;
import org.reco4j.engine.RecommenderEngine;
import org.reco4j.graph.neo4j.Neo4jGraph;
import org.reco4j.graph.neo4j.util.Neo4JPropertiesHandle;
import org.reco4j.recommender.IRecommender;
import org.reco4j.recommender.RecommenderEvaluator;

/**
 *
 * @author Luigi Giuri <luigi.giuri at reco4j.org>
 */
public class CollaborativeFilteringRecommenderTest extends TestCase
{
  public Properties initProperties;
  public Neo4jGraph learningDataSet;
  public Neo4jGraph testingDataSet;

  public CollaborativeFilteringRecommenderTest(String testName)
  {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

    initProperties = new Properties();
    initProperties.load(this.getClass().getResourceAsStream("collaborativeFilteringRecommenderInit.properties"));
    Neo4JPropertiesHandle propertiesHandle = new Neo4JPropertiesHandle(initProperties);
    
    learningDataSet = new Neo4jGraph(propertiesHandle);
    learningDataSet.initDatabase();

    testingDataSet = new Neo4jGraph(propertiesHandle);
    testingDataSet.setDatabase(learningDataSet.getGraphDB());
  }

  public void testBuildEvaluate()
  {
    IRecommender recommender = RecommenderEngine.createRecommender(initProperties);
//    recommender.loadRecommender(learningDataSet);
    recommender.buildRecommender(learningDataSet);
    RecommenderEvaluator.evaluateRecommender(testingDataSet, recommender);
  }
}
