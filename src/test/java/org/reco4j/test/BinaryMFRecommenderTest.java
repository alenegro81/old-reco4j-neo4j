/*
 * FastCollaborativeFilteringRecommenderBinaryTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.test;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import junit.framework.TestCase;
import org.reco4j.engine.RecommenderEngine;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.Neo4jGraph;
import org.reco4j.graph.neo4j.util.Neo4JPropertiesHandle;
import org.reco4j.recommender.IRecommender;
import org.reco4j.recommender.RecommenderEvaluator;

/**
 *
 * @author ale
 */
public class BinaryMFRecommenderTest extends TestCase
{
  public Properties initProperties;
  public Neo4jGraph learningDataSet;
  public Neo4jGraph testingDataSet;

  public BinaryMFRecommenderTest(String testName)
  {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

    initProperties = new Properties();
    initProperties.load(this.getClass().getResourceAsStream("binaryMFTest.properties"));
    Neo4JPropertiesHandle propertiesHandle = new Neo4JPropertiesHandle(initProperties);
    
    learningDataSet = new Neo4jGraph(propertiesHandle);
    learningDataSet.initDatabase();

    testingDataSet = new Neo4jGraph(propertiesHandle);
    testingDataSet.setDatabase(learningDataSet.getGraphDB());
  }

  public void testBuildEvaluate()
  {
    IRecommender recommender = RecommenderEngine.createRecommender(initProperties);
//    recommender.loadRecommender(learningDataSet);
    //recommender.setModelName("knn_model_user_follows");
    //recommender.buildRecommender(learningDataSet);
    recommender.setModelName("mf_model_asset_activated_jaccard");
    recommender.buildRecommender(learningDataSet);
    //FilteredUserItemDataset testUserItemDataset = new FilteredUserItemDataset();
    //testUserItemDataset.init(testingDataSet, RecommenderPropertiesHandle.getInstance(), filter);
    //String[] promotionIds = {"1331", "1399", "1194", "1315"};
    String[] promotionIds = {"650", "1221", "1194", "1331", "1399"};
    //ConcurrentHashMap<Long, INode> allUsers = learningDataSet.getNodesByQuery("START n = node(*) WHERE n.type! = 'User' and n.campagned! = \"yes\" return distinct n");
    for (String promotionId : promotionIds)
    {
      System.out.println("-------------------------- " + promotionId + " --------------------------");
      ConcurrentHashMap<Long, INode> usersProcessed = learningDataSet.getNodesByQuery("START n = node(*) MATCH (n)-[r:campagned]->(m) WHERE n.type! = 'User' and n.campagned! = \"yes\" and m.itemId = \"\\\"" + promotionId + "\\\"\" return distinct n");
      ConcurrentHashMap<Long, INode> usersHit = learningDataSet.getNodesByQuery("START n = node(*) MATCH (n)-[r:campagned]->(m) WHERE n.type! = 'User' and n.campagned! = \"yes\" and m.itemId = \"\\\"" + promotionId + "\\\"\" and r.accept = \"yes\" return distinct n");
      ConcurrentHashMap<Long, INode> items = learningDataSet.getNodesByQuery("START n = node(*) WHERE n.type! = 'Item' and n.itemId! = \"\\\"" + promotionId + "\\\"\" return n");
      INode item = items.values().iterator().next();
      System.out.println("Real value: " + usersHit.size() + "/" + usersProcessed.size() + " = " + ((double)usersHit.size() / (double)usersProcessed.size()) * 100 + "%");
//    INode item = learningDataSet.getNodeById(14);
//    for (INode user : usersHit.values())
//    {
//      System.out.println(recommender.estimateRating(user, item));
//    }
      RecommenderEvaluator.evaluateTopN(usersProcessed.values(), usersHit.values(), item, recommender, usersHit.size());
      
      //RecommenderEvaluator.evaluateTopNIntersection(allUsers.values(), usersProcessed.values(), item, recommender, usersProcessed.size(), "campagned");
      System.out.println("-------------------------- " + promotionId + " --------------------------");
    }
    //RecommenderEvaluator.evaluateTopN(learningDataSet.getItemNodes(), recommender);
    
    //0.94 - 1.5 cosine
    //0.94 - 1.80 jaccard
  }
}
