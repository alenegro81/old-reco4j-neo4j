/*
 * FastCollaborativeFilteringRecommenderBinaryAndFilteredTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.test;

import java.util.List;
import java.util.Properties;
import junit.framework.TestCase;
import org.reco4j.dataset.FilteredUserItemDataset;
import org.reco4j.engine.RecommenderEngine;
import org.reco4j.graph.neo4j.Neo4JNode;
import org.reco4j.graph.neo4j.Neo4jGraph;
import org.reco4j.graph.neo4j.filter.Neo4JPreFilter;
import org.reco4j.graph.neo4j.util.Neo4JPropertiesHandle;
import org.reco4j.model.Rating;
import org.reco4j.recommender.IRecommender;
import org.reco4j.util.RecommenderPropertiesHandle;

/**
 *
 * @author ale
 */
public class FastCollaborativeFilteringRecommenderBinaryAndFilteredTest extends TestCase
{
  public Properties initProperties;
  public Neo4jGraph learningDataSet;
  public Neo4jGraph testingDataSet;
  public Neo4JPropertiesHandle propertiesHandle;

  public FastCollaborativeFilteringRecommenderBinaryAndFilteredTest(String testName)
  {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

    initProperties = new Properties();
    initProperties.load(this.getClass().getResourceAsStream("binaryTest.properties"));
    propertiesHandle = new Neo4JPropertiesHandle(initProperties);
    
    learningDataSet = new Neo4jGraph(propertiesHandle);
    learningDataSet.initDatabase();

    testingDataSet = new Neo4jGraph(propertiesHandle);
    testingDataSet.setDatabase(learningDataSet.getGraphDB());
  }

  public void testBuildEvaluate()
  {
    IRecommender recommender = RecommenderEngine.createRecommender(initProperties);
    Neo4JPreFilter filter = new Neo4JPreFilter();
    filter.setItemsQuery("START n=node(*) where n.type?='asset' RETURN n;");
    //filter.setUsersQuery("START n=node(*) where n.type?='User' RETURN n;");
    filter.setUsersQuery("START n=node(*) where n.type?='user' RETURN n;");
    filter.setRatingsQuery("START r=relationship(*) where type(r)='LIKED' or type(r)='COMMENTED' RETURN r;");
    filter.setCommonsNodeQuery("START a=node($1) MATCH (a)<-[r1:LIKED|COMMENTED]-(x)-[r2:LIKED|COMMENTED]-(n) where x.type?='user' return distinct n;");
    FilteredUserItemDataset userItemDataset = new FilteredUserItemDataset();
    userItemDataset.init(learningDataSet, propertiesHandle, filter);    
    recommender.setModelName("knn_model_user_filtered_LIKED_COMMENTED");
    recommender.buildRecommender(learningDataSet, userItemDataset);
    List<Rating> recommendations = recommender.recommend(new Neo4JNode(1));
    
    for (Rating rate : recommendations)
    {
      //Do something
      System.out.println("Node: " + rate.getItemID() + " - " + rate.getRate());
    }
    
    
    //FilteredUserItemDataset testUserItemDataset = new FilteredUserItemDataset();
    //testUserItemDataset.init(testingDataSet, RecommenderPropertiesHandle.getInstance(), filter);  
    //RecommenderEvaluator.evaluateRecommender(testUserItemDataset, recommender);
  }
}
