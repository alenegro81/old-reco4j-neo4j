/*
 * FastFilteredCollaborativeFilteringRecommenderTest.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.test;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import junit.framework.TestCase;
import org.reco4j.dataset.FilteredUserItemDataset;
import org.reco4j.engine.RecommenderEngine;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.Neo4JNode;
import org.reco4j.graph.neo4j.Neo4jGraph;
import org.reco4j.graph.neo4j.filter.Neo4JPostFilter;
import org.reco4j.graph.neo4j.filter.Neo4JPreFilter;
import org.reco4j.graph.neo4j.util.Neo4JPropertiesHandle;
import org.reco4j.recommender.IRecommender;
import org.reco4j.recommender.RecommenderEvaluator;
import org.reco4j.util.RecommenderPropertiesHandle;

/**
 *
 * @author Luigi Giuri <luigi.giuri at reco4j.org>
 */
public class FastFilteredCollaborativeFilteringRecommenderTest extends TestCase
{

  //public Properties initProperties;
  //public Neo4jGraph learningDataSet;
  public Neo4jGraph testingDataSet;

  public FastFilteredCollaborativeFilteringRecommenderTest(String testName)
  {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

  }

  public void testBuildEvaluate() throws IOException
  {
    /* 
     * Import init properties
     */
    Properties initProperties = new Properties();
    initProperties.load(this.getClass().getResourceAsStream("fastcollaborativeFilteringRecommenderInit.properties"));
    Neo4JPropertiesHandle propertiesHandle = new Neo4JPropertiesHandle(initProperties);
    /*
     * Configure and initialize graph database connection
     */
    Neo4jGraph learningDataSet = new Neo4jGraph(propertiesHandle);
    learningDataSet.initDatabase();
    /*
     * Create the recommender
     */    
    IRecommender recommender = RecommenderEngine.createRecommender(initProperties);
    /*
    * Create and configure prefilter
    */
    Neo4JPreFilter preFilter = new Neo4JPreFilter();
    preFilter.setItemsQuery("START n=node(*) where n.type?='Movie' RETURN n;");
    preFilter.setUsersQuery("START n=node(*) where n.type?='User' and n.gender?='M' and n.age ?> 30 RETURN n;");
    preFilter.setRatingsQuery("START r=relationship(*) where type(r)='rated' RETURN r;");
    preFilter.setCommonsNodeQuery("START a=node($1) MATCH (a)<-[r1:rated]-(x)-[r2:rated]-(n) "
            + " where x.type?='User' and x.gender?='M' and x.age ?> 30 return distinct n;");
    /*
    * Create and configure postfilter
    */
    Neo4JPostFilter postFilter = new Neo4JPostFilter();
    postFilter.setItemsQuery("START n=node(*) where n.type?='Movie' and n.year >= 2011 RETURN n;"); 
    /*
     * Create and initialize the userItem data set
     */
    FilteredUserItemDataset userItemDataset = new FilteredUserItemDataset();
    userItemDataset.init(learningDataSet, propertiesHandle, preFilter, postFilter);
    /*
     * Configure the model name and build the recommender
     */
    recommender.setModelName("knn_model_user_filtered_euclidean_man_gt_30");
    recommender.buildRecommender(learningDataSet, userItemDataset);
    
    INode user = new Neo4JNode(2);
    List<INode> recommendation = recommender.recommend(user);
    
    for (INode movie : recommendation)
      System.out.println("Movie: " + movie.getProperty("title"));
    
            
    testingDataSet = new Neo4jGraph(propertiesHandle);
    testingDataSet.setDatabase(learningDataSet.getGraphDB());
    
    FilteredUserItemDataset testUserItemDataset = new FilteredUserItemDataset();
    testUserItemDataset.init(testingDataSet, propertiesHandle, preFilter);
    RecommenderEvaluator.evaluateRecommender(testUserItemDataset, recommender);
  }
}
