/*
 * Neo4JPostFilter.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.filter;

import java.util.concurrent.ConcurrentHashMap;
import org.reco4j.graph.IGraph;
import org.reco4j.graph.INode;
import org.reco4j.filter.IPostFilter;
import org.reco4j.graph.neo4j.Neo4jGraph;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JPostFilter implements IPostFilter
{

  private Neo4jGraph graph;
  private String itemsQuery;
  
  @Override
  public void setGraph(IGraph graph)
  {
    if (graph instanceof Neo4jGraph)
      this.graph = (Neo4jGraph) graph;
    else
      throw new RuntimeException("Try to initialize Neo4JFilter with a graph that is not an instance of Neo4jGraph");
  }

  @Override
  public ConcurrentHashMap<Long, INode> getItemNodesMap()
  {
    return graph.getNodesByQuery(itemsQuery);
  }

  public void setItemsQuery(String itemsQuery)
  {
    this.itemsQuery = itemsQuery;
  }
  
}
