/*
 * Neo4JPreFilter.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.filter;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.mahout.cf.taste.impl.common.FastIDSet;
import org.reco4j.graph.IEdge;
import org.reco4j.graph.IGraph;
import org.reco4j.graph.INode;
import org.reco4j.filter.IPreFilter;
import org.reco4j.graph.neo4j.Neo4jGraph;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JPreFilter implements IPreFilter
{

  private Neo4jGraph graph;
  private String usersQuery;
  private String itemsQuery;
  private String ratingsQuery;
  private String commonsNodeQuery;
  
  public void setGraph(IGraph graph)
  {
    if (graph instanceof Neo4jGraph)
      this.graph = (Neo4jGraph) graph;
    else
      throw new RuntimeException("Try to initialize Neo4JFilter with a graph that is not an instance of Neo4jGraph");
  }

  public ConcurrentHashMap<Long, INode> getItemNodesMap()
  {
    return graph.getNodesByQuery(itemsQuery);
  }

  public ConcurrentHashMap<Long, INode> getUserNodesMap()
  {
    return graph.getNodesByQuery(usersQuery);
  }

  public List<IEdge> getRatingList()
  {
    return graph.getEdgesByQuery(ratingsQuery);
  }

  public FastIDSet getCommonNodeIds(INode item)
  {
    String replacedString = commonsNodeQuery.replace("$1", "" + item.getId());
    return graph.getNodesIdByQuery(replacedString);
  }

  public FastIDSet getItemNodesId()
  {
    return graph.getNodesIdByQuery(itemsQuery);
  }

  public void setUsersQuery(String usersQuery)
  {
    this.usersQuery = usersQuery;
  }

  public void setItemsQuery(String itemsQuery)
  {
    this.itemsQuery = itemsQuery;
  }

  public void setRatingsQuery(String ratingsQuery)
  {
    this.ratingsQuery = ratingsQuery;
  }

  public void setCommonsNodeQuery(String commonsNodeQuery)
  {
    this.commonsNodeQuery = commonsNodeQuery;
  }

  public ConcurrentHashMap<Long, INode> getCommonNodes(INode item)
  {
    String replacedString = commonsNodeQuery.replace("$1", "" + item.getId());
    return graph.getNodesByQuery(replacedString);
  }
  
}
