/*
 * Reco4jPluginLifecycle.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.plugin;

import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import org.apache.commons.configuration.Configuration;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.server.plugins.Injectable;
import org.neo4j.server.plugins.PluginLifecycle;
import org.reco4j.graph.neo4j.engine.RecommenderNeo4jEngineManager;
import org.reco4j.graph.neo4j.plugin.handler.Reco4jEventHandler;

/**
 * add this into neo4j-server.properties
 * org.neo4j.server.thirdparty_jaxrs_classes=org.reco4j.graph.neo4j.plugin=/reco4jplugin/unmanaged
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
@Path( "/reco4j/lifecycle" )
public class Reco4jPluginLifecycle implements PluginLifecycle
{
  public Collection<Injectable<?>> start(GraphDatabaseService gds, Configuration c)
  {
    Logger.getLogger(Reco4jPluginLifecycle.class.getName()).log(Level.WARNING, ">>>>>>>>>>>>>>>>>>>>>>>>>>: Reco4jPluginLifecycle ... ... start!");
    RecommenderNeo4jEngineManager.getInstance().init(gds, c);
    RecommenderNeo4jEngineManager.getInstance().startRecommender();
    return Collections.<Injectable<?>>singleton(new Injectable<RecommenderNeo4jEngineManager>()
    {
      @Override
      public RecommenderNeo4jEngineManager getValue()
      {
        Logger.getLogger(Reco4jPluginLifecycle.class.getName()).log(Level.WARNING, ">>>>>>>>>>>>>>>>>>>>>>>>>>: Reco4jPluginLifecycle ... ... getValue!");
        return RecommenderNeo4jEngineManager.getInstance();
      }
      @Override
      public Class<RecommenderNeo4jEngineManager> getType()
      {
        Logger.getLogger(Reco4jPluginLifecycle.class.getName()).log(Level.WARNING, ">>>>>>>>>>>>>>>>>>>>>>>>>>: Reco4jPluginLifecycle ... ... getType!");
        return RecommenderNeo4jEngineManager.class;
      }
    });
  }

  public void stop()
  {
    Logger.getLogger(Reco4jPluginLifecycle.class.getName()).log(Level.WARNING, "Reco4jPluginLifecycle ... ... stop!");
  }
}
