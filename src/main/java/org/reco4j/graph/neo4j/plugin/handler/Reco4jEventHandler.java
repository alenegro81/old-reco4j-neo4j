/*
 * Reco4jEventHandler.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.plugin.handler;

import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.event.PropertyEntry;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.graphdb.event.TransactionEventHandler;
import org.reco4j.graph.IGraphUpdateOperationConstants;
import org.reco4j.graph.neo4j.engine.RecommenderNeo4jEngineManager;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */

public class Reco4jEventHandler implements TransactionEventHandler<Object>
{
  
  @Override
  public void afterCommit(TransactionData data, Object state)
  {
    for (Relationship entry : data.createdRelationships())
    {
      RecommenderNeo4jEngineManager.getInstance().updateRecommender(entry, IGraphUpdateOperationConstants.GRAPH_OPERATION_ADD_EDGE);
    }
    for (Relationship entry : data.deletedRelationships())
    {
      RecommenderNeo4jEngineManager.getInstance().updateRecommender(entry, IGraphUpdateOperationConstants.GRAPH_OPERATION_REMOVE_EDGE);
    }
    for (PropertyEntry<Relationship> entry : data.removedRelationshipProperties())
    {
      RecommenderNeo4jEngineManager.getInstance().updateRecommender(entry.entity(), IGraphUpdateOperationConstants.GRAPH_OPERATION_REMOVE_EDGE_PROPERTY);
    }
    for (PropertyEntry<Relationship> entry : data.assignedRelationshipProperties())
    {
      RecommenderNeo4jEngineManager.getInstance().updateRecommender(entry.entity(), IGraphUpdateOperationConstants.GRAPH_OPERATION_ADD_EDGE_PROPERTY);
    }
    
  }

  @Override
  public void afterRollback(TransactionData data, Object state)
  {
    //donothing
  }

  @Override
  public Object beforeCommit(TransactionData data)
    throws Exception
  {
    // TODO Auto-generated method stub
    return null;
  }
}
