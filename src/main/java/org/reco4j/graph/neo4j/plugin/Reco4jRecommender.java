/*
 * Reco4jRecommender.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.plugin;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.server.plugins.*;
import org.reco4j.graph.neo4j.Neo4JNode;
import org.reco4j.graph.neo4j.engine.RecommenderNeo4jEngineManager;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
@Description("An extension to the Neo4j Server for recommendation getting from reco4j")
public class Reco4jRecommender extends ServerPlugin
{

  @Name("get_recommendations")
  @Description("Get recommendation for a user")
  @PluginTarget(Node.class)
  public Iterable<Node> getRecommendations(@Source Node source,
                                           @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    Logger.getLogger(Reco4jRecommender.class.getName()).log(Level.WARNING, "Reco4jRecommender ... ... chiamato!");
    return RecommenderNeo4jEngineManager.getInstance().getRecommender(new Neo4JNode(source), recommender);
  }

  @Name("get_users")
  @Description("Get all users managed by the recommender accrodingly to its configuration")
  @PluginTarget(GraphDatabaseService.class)
  public Iterable<Node> getAllNodes(@Source GraphDatabaseService graphDb,
                                    @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getUsers(recommender);
  }

  @Name("get_items")
  @Description("Get all item managed by the recommender accrodingly to its configuration")
  @PluginTarget(GraphDatabaseService.class)
  public Iterable<Node> getItems(@Source GraphDatabaseService graphDb,
                                 @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getItems(recommender);
  }

  @Name("get_all_ratings")
  @Description("Get all ratings involved into the recommendation process accrodingly to its configuration")
  @PluginTarget(GraphDatabaseService.class)
  public Iterable<Relationship> getAllRatings(@Source GraphDatabaseService graphDb,
                                              @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getAllRatings(recommender);
  }

  @Name("get_ratings")
  @Description("Get ratings given by a user")
  @PluginTarget(Node.class)
  public Iterable<Relationship> getRatings(@Source Node source,
                                           @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getRatings(source, recommender);
  }

  @Name("get_prediction")
  @Description("Get ratings given by a user")
  @PluginTarget(Node.class)
  public double getPrediction(@Source Node source,
                              @Parameter(name = "item", optional = true) Node item,
                              @Description("The name of recommender to use")
          @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getPrediction(source, item, recommender);
  }

  @Name("get_similarity")
  @Description("Get ratings given by a user")
  @PluginTarget(Node.class)
  public Iterable<Node> getSimilarity(@Source Node source,
                                              @Parameter(name = "cardinality", optional = false) int cardinality,
                                              @Description("The name of recommender to use") @Parameter(name = "recommender", optional = true) String recommender)
  {
    return RecommenderNeo4jEngineManager.getInstance().getSimilarItems(source, cardinality, recommender);
  }
//  @Name( "get_testratings")
//  @Description( "Get test ratings given by a user")
//  @PluginTarget( Node.class)
//  public Iterable<Relationship> getTestRatings(@Source Node source)
//  {
//    return RecommenderNeo4jEngineManager.getInstance().getRatingsTest(source);
//  }
}
