/*
 * Neo4JEdge.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.reco4j.graph.neo4j;

import java.lang.ref.SoftReference;
import java.util.Iterator;
import org.neo4j.graphdb.Relationship;
import org.reco4j.graph.BasicEdge;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.traversal.GraphTraversal;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JEdge extends BasicEdge
{
  private SoftReference<Relationship> edgeRef;
  private long id;
  
  public Neo4JEdge(Relationship edge)
  {
    setEdge(edge);
  }
  
  @Override
  public void setProperty(String name, Object value)
  {
    GraphTraversal.getInstance().setEdgeProperty(this, name, value);
  }

  @Override
  public String getProperty(String name)
  {
    String value = getPropertyValue(name);
    if (value == null)
    {
      StringBuilder exception = new StringBuilder();
      exception.append("getProperty conversion data not supported! Not available property '").append(name).append("'on node!\n");
      exception.append("Available properties are:\n");
      for (Iterator<String> it = getEdge().getPropertyKeys().iterator(); it.hasNext();)
      {
        String key = it.next();
        exception.append(key).append("\n");
      }
      throw new RuntimeException(exception.toString());
    }
    return value;
      
  }

  @Override
  public INode getSource()
  {
    return new Neo4JNode(getEdge().getStartNode());
  }

  @Override
  public INode getDestination()
  {
    return new Neo4JNode(getEdge().getEndNode());
  }

  public Relationship getEdge()
  {
    Relationship edge = edgeRef.get();
    if (edge == null)
    {
      edge = GraphTraversal.getInstance().getBaseEdgeById(id);
      setEdge(edge);
    }
    return edge;
  }

  public final void setEdge(Relationship edge)
  {
    id = edge.getId();
    this.edgeRef = new SoftReference<Relationship>(edge);
  }


  @Override
  public String getPermissiveProperty(String name)
  {
    if (!getEdge().hasProperty(name))
      return null;
    return getPropertyValue(name);
  }

  public long getId()
  {
    return id;
  }

  private String getPropertyValue(String name) throws RuntimeException
  {
    Object value = getEdge().getProperty(name);
    if (value == null)
        return null;
    if (value instanceof String)
      return (String) value;
    else if (value instanceof Integer)
      return ((Integer) value).toString();
    else if (value instanceof Float)
      return ((Float) value).toString();
    else if (value instanceof Long)
      return ((Long) value).toString();
    else if (value instanceof Double)
      return ((Double) value).toString();
    else
      throw new RuntimeException("getProperty conversion data not supported! Not supported data type for property '" + name + "': " + value.getClass());

  }

  public void setId(long id)
  {
    this.id = id;
  }
}
