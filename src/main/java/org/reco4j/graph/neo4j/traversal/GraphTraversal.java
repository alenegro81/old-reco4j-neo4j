/*
 * Neo4jGraph.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.traversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mahout.cf.taste.impl.common.FastIDSet;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.index.RelationshipIndex;
import org.neo4j.helpers.collection.IteratorUtil;
import org.neo4j.tooling.GlobalGraphOperations;
import org.reco4j.graph.IEdge;
import org.reco4j.graph.IEdgeType;
import org.reco4j.graph.IGraphCallable;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.Neo4JEdge;
import org.reco4j.graph.neo4j.Neo4JNode;
import org.reco4j.graph.neo4j.util.INeo4JGraphConfig;
import org.reco4j.util.RecommenderPropertiesHandle;

/**
 * Hello world!
 *
 */
public class GraphTraversal
{

  private static final Logger logger = Logger.getLogger(GraphTraversal.class.getName());
  private static GraphDatabaseService graphDB;

  private static class GraphTraversalHolder
  {

    private static final GraphTraversal INSTANCE = new GraphTraversal();
  }

  public static GraphTraversal getInstance()
  {
    return GraphTraversal.GraphTraversalHolder.INSTANCE;
  }

  public void init(GraphDatabaseService graphDB)
  {
    GraphTraversal.graphDB = graphDB;
  }

  public List<INode> getNodesByType(String type, String typePropertyName)
  {
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      return getNodeList(result);
    } else
    {
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      return getNodeList(allNodes, type, typePropertyName);
    }
  }

  public FastIDSet getNodesIdByType(String type, String typePropertyName)
  {
    FastIDSet ids = new FastIDSet();
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      while (result.hasNext())
        ids.add(result.next().getId());
    } else
    {
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      for (Node next : allNodes)
        if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
          ids.add(next.getId());
    }
    return ids;
  }

  public ConcurrentHashMap<Long, INode> getNodesMapByType(String type, String typePropertyName)
  {
    ConcurrentHashMap<Long, INode> nodes = new ConcurrentHashMap<Long, INode>();
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      while (result.hasNext())
      {
        Node item = result.next();
        INode node = new Neo4JNode(item);
        nodes.put(node.getId(), node);
      }
      return nodes;
    } else
    {
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      for (Node next : allNodes)
      {
        if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
        {
          INode node = new Neo4JNode(next);
          nodes.put(node.getId(), node);
        }
      }
      return nodes;
    }

  }

  public void getNodesByType(String type, String typePropertyName, IGraphCallable<INode> callback)
  {
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      while (result.hasNext())
      {
        Neo4JNode node = new Neo4JNode(result.next());
        callback.call(node);
      }
    } else
    {
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      for (Node next : allNodes)
      {
        if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
        {
          Neo4JNode node = new Neo4JNode(next);
          callback.call(node);
        }
      }
    }
  }

  /**
   *
   * @param query must return a node column named 'n'
   */
  public ConcurrentHashMap<Long, INode> getNodesByQuery(String query)
  {
    ConcurrentHashMap<Long, INode> result = new ConcurrentHashMap<Long, INode>();
    ExecutionEngine engine = new ExecutionEngine(graphDB);
    ExecutionResult resultSet = engine.execute(query);

    Iterator<Node> allNodes = resultSet.columnAs("n");
    for (Node next : IteratorUtil.asIterable(allNodes))
    {
      Neo4JNode node = new Neo4JNode(next);
      result.put(node.getId(), node);
    }
    return result;
  }
  
  public FastIDSet getNodesIdByQuery(String query)
  {
    FastIDSet result = new FastIDSet();
    ExecutionEngine engine = new ExecutionEngine(graphDB);
    ExecutionResult resultSet = engine.execute(query);

    Iterator<Node> allNodes = resultSet.columnAs("n");
    for (Node next : IteratorUtil.asIterable(allNodes))
      if (next.getId() > 0)
        result.add(next.getId());
    
    return result;
  }
  /**
   *
   * @param query must return relationships column named 'r'
   */
  public List<IEdge> getEdgesByQuery(String query)
  {
    List<IEdge> result = new ArrayList<IEdge>();
    ExecutionEngine engine = new ExecutionEngine(graphDB);
    ExecutionResult resultSet = engine.execute(query);

    Iterator<Relationship> allRelationships = resultSet.columnAs("r");
    for (Relationship next : IteratorUtil.asIterable(allRelationships))
    {
      Neo4JEdge edge = new Neo4JEdge(next);
      result.add(edge);
    }
    return result;
  }

  public List<IEdge> getEdgesByType(IEdgeType edgeType)
  {
    String type = edgeType.getEdgeName();
    DynamicRelationshipType relType = DynamicRelationshipType.withName(type);

    List<IEdge> edges = new ArrayList();

    if (graphDB.index().existsForRelationships("reco4j_idx_rel_type"))
    {
      RelationshipIndex relIndex = graphDB.index().forRelationships("reco4j_idx_rel_type");
      IndexHits<Relationship> relations = relIndex.get("type", relType.name());
      while (relations.hasNext())
      {
        Relationship item = relations.next();
        IEdge edge = new Neo4JEdge(item);
        edges.add(edge);
      }

    } else
    {
      Iterable<Relationship> allRelationships = GlobalGraphOperations.at(graphDB).getAllRelationships();

      for (Relationship next : allRelationships)
      {
        if (next.isType(relType))
        {
          IEdge edge = new Neo4JEdge(next);
          edges.add(edge);
        }
      }
    }
    return edges;
  }

  public FastIDSet getEdgesIdByType(IEdgeType edgeType)
  {
    String type = edgeType.getEdgeName();
    DynamicRelationshipType relType = DynamicRelationshipType.withName(type);
    FastIDSet edges = new FastIDSet();
    if (graphDB.index().existsForRelationships("reco4j_idx_rel_type"))
    {
      RelationshipIndex relIndex = graphDB.index().forRelationships("reco4j_idx_rel_type");
      IndexHits<Relationship> relations = relIndex.get("type", relType.name());
      while (relations.hasNext())
        edges.add(relations.next().getId());
    } else
    {
      Iterable<Relationship> allRelationships = GlobalGraphOperations.at(graphDB).getAllRelationships();
      for (Relationship next : allRelationships)
        if (next.isType(relType))
          edges.add(next.getId());
    }
    return edges;
  }

  public List<INode> getNodesByInEdge(IEdgeType edgesType)
  {
    Iterable<Relationship> allRelationships = GlobalGraphOperations.at(graphDB).getAllRelationships();
    List<INode> nodes = new ArrayList();
    RelationshipType relationshipType = getRelationshipTypeByEdgeType(edgesType);

    for (Relationship next : allRelationships)
    {
      if (next.isType(relationshipType))
      {
        Node item = next.getEndNode();
        INode node = new Neo4JNode(item);
        nodes.add(node);
      }
    }
    return nodes;
  }

  public List<IEdge> getInEdge(INode inode, IEdgeType edgesType)
  {
    //Node nodeTmp = inode.
    Node nodeTmp = ((Neo4JNode) inode).getNode();
    Iterable<Relationship> edgeList = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.INCOMING);
    return getIEdgeList(edgeList);
  }

  public FastIDSet getInEdgeIds(INode inode, IEdgeType edgesType)
  {
    Node nodeTmp = ((Neo4JNode) inode).getNode();
    Iterable<Relationship> edgeList = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.INCOMING);
    return getIEdgeSet(edgeList);
  }

  public List<IEdge> getOutEdge(INode inode, IEdgeType edgesType)
  {
    Node nodeTmp = ((Neo4JNode) inode).getNode();;
    Iterable<Relationship> edgeList = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.OUTGOING);
    return getIEdgeList(edgeList);
  }

  public Boolean isConnected(INode x, INode y, IEdgeType edgesType)
  {
    Node a = ((Neo4JNode) x).getNode(); //temporaneo
    Node b = ((Neo4JNode) y).getNode(); //temporaneo
    Iterable<Relationship> edgeList = a.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.BOTH);
    for (Relationship edge : edgeList)
    {
      Node otherNode = edge.getOtherNode(a);
      if (otherNode.getId() == b.getId())
        return true;
    }
    return false;
  }

  public IEdge getEdge(INode x, INode y, IEdgeType edgesType)
  {
    Node a = ((Neo4JNode) x).getNode();
    Node b = ((Neo4JNode) y).getNode();
    Iterable<Relationship> edgeList = a.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.BOTH);
    for (Relationship edge : edgeList)
      if (edge.getOtherNode(a).getId() == b.getId())
        return new Neo4JEdge(edge);
    return null;
  }

  public ConcurrentHashMap<Long, INode> getCommonNodes(INode node, IEdgeType edgesType, String identifier)
  {
    ConcurrentHashMap<Long, INode> nodes = new ConcurrentHashMap<Long, INode>();//Sostituire con la fast
    Node nodeTmp = ((Neo4JNode) node).getNode();
    String nodeIdentifier = node.getProperty(identifier);
    Iterable<Relationship> inRelationships = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.INCOMING);
    for (Relationship inRelationship : inRelationships)
    {
      Node source = inRelationship.getStartNode();
      Iterable<Relationship> outRelationships = source.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.OUTGOING);
      for (Relationship outRelationship : outRelationships)
      {
        Node dest = outRelationship.getEndNode();
        String destIdentifier = new Neo4JNode(dest).getProperty(identifier);

        if (!(destIdentifier.equalsIgnoreCase(nodeIdentifier))
                && !nodes.containsKey(destIdentifier))
          nodes.put(dest.getId(), new Neo4JNode(dest));
      }
    }
    return nodes;
  }

  public FastIDSet getCommonNodeIds(INode node, IEdgeType edgesType)
  {
    FastIDSet nodes = new FastIDSet();
    Node nodeTmp = ((Neo4JNode) node).getNode();
    Iterable<Relationship> inRelationships = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.INCOMING);
    for (Relationship inRelationship : inRelationships)
    {
      Node source = inRelationship.getStartNode();
      Iterable<Relationship> outRelationships = source.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.OUTGOING);
      for (Relationship outRelationship : outRelationships)
        if (outRelationship.getEndNode().getId() != nodeTmp.getId())
          nodes.add(outRelationship.getEndNode().getId());
    }
    return nodes;
  }

  public void getCommonNodes(INode node, IEdgeType edgesType, IGraphCallable<INode> callback)
  {
    HashMap<Long, INode> nodes = new HashMap<Long, INode>();//Sostituire con la fast
    Node nodeTmp = ((Neo4JNode) node).getNode();
    Iterable<Relationship> inRelationships = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.INCOMING);
    for (Relationship inRelationship : inRelationships)
    {
      Node source = inRelationship.getStartNode();
      Iterable<Relationship> outRelationships = source.getRelationships(getRelationshipTypeByEdgeType(edgesType), Direction.OUTGOING);
      for (Relationship outRelationship : outRelationships)
      {
        Node dest = outRelationship.getEndNode();
        if (dest.getId() != nodeTmp.getId() && !nodes.containsKey(dest.getId()))
        {
          Neo4JNode neo4JNode = new Neo4JNode(dest);
          nodes.put(dest.getId(), neo4JNode);
          callback.call(neo4JNode);
        }
      }
    }
  }

  public void addEdge(INode x, INode y, IEdgeType similarityEdgeType, String propertyName, Object value)
  {
    Node source = ((Neo4JNode) x).getNode();
    Node dest = ((Neo4JNode) y).getNode();
    Transaction tx = graphDB.beginTx();
    try
    {
      Relationship createRelationshipTo = source.createRelationshipTo(dest, getRelationshipTypeByEdgeType(similarityEdgeType));
      createRelationshipTo.setProperty(propertyName, value);
      tx.success();
    } catch (Exception ex)
    {
      tx.failure();
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error during addEdgeTransaction: ", ex);
    } finally
    {
      tx.finish();
    }
  }

  public void addEdge(INode x, INode y, IEdgeType edgeType)
  {
    Node source = ((Neo4JNode) x).getNode();
    Node dest = ((Neo4JNode) y).getNode();
    Transaction tx = graphDB.beginTx();
    try
    {
      Relationship createRelationshipTo = source.createRelationshipTo(dest, getRelationshipTypeByEdgeType(edgeType));
      tx.success();
    } catch (Exception ex)
    {
      tx.failure();
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error during addEdgeTransaction: ", ex);
    } finally
    {
      tx.finish();
    }
  }

  public void setEdgeProperty(IEdge edge, String propertyName, Object value)
  {
    Relationship rel = ((Neo4JEdge) edge).getEdge();
    Transaction tx = graphDB.beginTx();
    try
    {
      rel.setProperty(propertyName, value);
      tx.success();
    } catch (Exception ex)
    {
      tx.failure();
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error during addEdgeTransaction: ", ex);
    } finally
    {
      tx.finish();
    }
  }

  public int getInEdgeNumber(INode node, IEdgeType edgeType)
  {
    Node nodeTmp = ((Neo4JNode) node).getNode();
    Iterable<Relationship> edgeList = nodeTmp.getRelationships(getRelationshipTypeByEdgeType(edgeType), Direction.INCOMING);
    return getIEdgeSet(edgeList).size();
  }

  private List<IEdge> getIEdgeList(Iterable<Relationship> allRelationships)
  {
    List<IEdge> edges = new ArrayList();
    for (Relationship next : allRelationships)
    {
      IEdge edge = new Neo4JEdge(next);
      edges.add(edge);
    }
    return edges;
  }

  private FastIDSet getIEdgeSet(Iterable<Relationship> allRelationships)
  {
    FastIDSet edges = new FastIDSet();
    for (Relationship next : allRelationships)
      edges.add(next.getId());
    return edges;
  }

  private List<INode> getNodeList(IndexHits<Node> result)
  {
    List<INode> nodes = new ArrayList();
    while (result.hasNext())
    {
      INode node = new Neo4JNode(result.next());
      nodes.add(node);
    }
    return nodes;
  }

  private List<INode> getNodeList(Iterable<Node> allNodes, String type, String typePropertyName)
  {
    List<INode> nodes = new ArrayList<INode>();
    for (Node next : allNodes)
    {
      if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
      {
        INode node = new Neo4JNode(next);
        nodes.add(node);
      }
    }
    return nodes;
  }

  private RelationshipType getRelationshipTypeByEdgeType(IEdgeType edgesType)
  {
    String edges = edgesType.getEdgeName();
    RelationshipType relType = DynamicRelationshipType.withName(edges);
    return relType;
  }

  public void iterateOnEdge(Neo4JNode node, IEdgeType edgeType, IGraphCallable<IEdge> callback)
  {
    Node neo4jNode = ((Neo4JNode) node).getNode();
    Iterable<Relationship> edgeList = neo4jNode.getRelationships(getRelationshipTypeByEdgeType(edgeType));
    for (Relationship next : edgeList)
    {
      IEdge edge = new Neo4JEdge(next);
      callback.call(edge);
    }
  }

  public int getNodesNumberByType(String type, String typePropertyName)
  {
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      return result.size();
    } else
    {
      int count = 0;
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      for (Node next : allNodes)
        if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
          count++;
      return count;
    }
  }
  //This method works for now only with identifier Long or Integer since it is used only in mahout

  public INode getNodeById(long id, String type, String identifier, String typePropertyName)
  {
    //TODO: Introdurre autoindexing!
    //http://docs.neo4j.org/chunked/stable/auto-indexing.html
    if (graphDB.index().existsForNodes("idx_node_type"))
    {
      Index<Node> idx_node_type = graphDB.index().forNodes("idx_node_type");
      IndexHits<Node> result = idx_node_type.get(typePropertyName, type);
      while (result.hasNext())
      {
        Node next = result.next();
        Integer nodeId = (Integer) next.getProperty(identifier);
        if (nodeId.longValue() == id)
          return new Neo4JNode(next);
      }
    } else
    {
      Iterable<Node> allNodes = GlobalGraphOperations.at(graphDB).getAllNodes();
      for (Node next : allNodes)
        if (((String) next.getProperty(typePropertyName)).equalsIgnoreCase(type))
        {
          Integer nodeId = (Integer) next.getProperty(identifier);
          if (nodeId.longValue() == id)
            return new Neo4JNode(next);
        }



    }
    return null;
  }

  public void optimizeGraph(boolean force)
  {

    final String reco4j_idx_rel_type = "reco4j_idx_rel_type";
    if (force)
    {
      if (graphDB.index().existsForRelationships(reco4j_idx_rel_type))
      {
        logger.warning("Deleting index!");
        Transaction tx = graphDB.beginTx();
        try
        {
          graphDB.index().forRelationships(reco4j_idx_rel_type).delete();
          tx.success();
        } catch (Exception ex)
        {
          tx.failure();
          logger.log(Level.SEVERE, "Error during deleting relationship index: " + reco4j_idx_rel_type, ex);
        } finally
        {
          tx.finish();
        }
      }

    }
    if (!graphDB.index().existsForRelationships(reco4j_idx_rel_type))
    {
      logger.warning("Index on Relationship: " + reco4j_idx_rel_type + " not present. Let me add it!");
      Transaction tx = graphDB.beginTx();
      try
      {
        RelationshipIndex index_on_type = graphDB.index().forRelationships("reco4j_idx_rel_type");
        Iterable<Relationship> allRelationships = GlobalGraphOperations.at(graphDB).getAllRelationships();
        int count = 0;
        for (Relationship next : allRelationships)
        {
          index_on_type.add(next, "type", next.getType().name());
          count++;
          if (count % 1000 == 0)
          {
            tx.success();
            tx.finish();
            tx = graphDB.beginTx();
          }
        }
        tx.success();
      } catch (Exception ex)
      {
        tx.failure();
        logger.log(Level.SEVERE, "Error during add relationship index: ", ex);
      } finally
      {
        tx.finish();
      }
    }
  }

  public Node getBaseNodeById(long id)
  {
    return graphDB.getNodeById(id);
  }

  public Relationship getBaseEdgeById(long id)
  {
    return graphDB.getRelationshipById(id);
  }

  public INode addNode(Map<String, String> properties)
  {
    INode node = null;
    Transaction tx = graphDB.beginTx();
    try
    {
      Node newNode = graphDB.createNode();
      for (String property : properties.keySet())
        newNode.setProperty(property, properties.get(property));
      node = new Neo4JNode(newNode);
      tx.success();
    } catch (Exception ex)
    {
      tx.failure();
      logger.log(Level.SEVERE, "Error during node creation: ", ex);
    } finally
    {
      tx.finish();
    }
    return node;
  }

  public void setNodeProperty(INode node, String propertyName, String value)
  {
    Transaction tx = graphDB.beginTx();
    try
    {
      Node newNode = ((Neo4JNode) node).getNode();
      newNode.setProperty(propertyName, value);
      tx.success();
    } catch (Exception ex)
    {
      tx.failure();
      logger.log(Level.SEVERE, "Error during node property setting: ", ex);
    } finally
    {
      tx.finish();
    }
  }
}
