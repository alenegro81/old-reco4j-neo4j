/*
 * Neo4jGraph.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mahout.cf.taste.impl.common.FastIDSet;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.reco4j.graph.IEdge;
import org.reco4j.graph.IEdgeType;
import org.reco4j.graph.IGraph;
import org.reco4j.graph.IGraphCallable;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.plugin.handler.Reco4jEventHandler;
import org.reco4j.graph.neo4j.traversal.GraphTraversal;
import org.reco4j.graph.neo4j.util.INeo4JGraphConfig;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4jGraph implements IGraph
{

  private GraphDatabaseService graphDB;
  private static final Logger logger = Logger.getLogger(Neo4jGraph.class.getName());
  private final INeo4JGraphConfig neo4JGraphConfig;

  public Neo4jGraph(INeo4JGraphConfig neo4JGraphConfig)
  {
    this.neo4JGraphConfig = neo4JGraphConfig;
  }

  public void initDatabase()
  {
    Map<String, String> graphDatabaseBuilderConfig = new HashMap<String, String>();
    graphDatabaseBuilderConfig.put("use_memory_mapped_buffers", "false");
    graphDatabaseBuilderConfig.put("neostore.nodestore.db.mapped_memory", "800M");
    graphDatabaseBuilderConfig.put("neostore.relationshipstore.db.mapped_memory", "100M");
    graphDatabaseBuilderConfig.put("neostore.propertystore.db.mapped_memory", "100M");
    graphDatabaseBuilderConfig.put("neostore.propertystore.db.index.mapped_memory", "1M");
    graphDatabaseBuilderConfig.put("neostore.propertystore.db.index.keys.mapped_memory", "1M");
    graphDatabaseBuilderConfig.put("neostore.propertystore.db.strings.mapped_memory", "150M");
    graphDatabaseBuilderConfig.put("neostore.propertystore.db.arrays.mapped_memory", "0M");
    GraphDatabaseService newGraphDatabase =
            new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(neo4JGraphConfig.getDBPath()).
            setConfig(graphDatabaseBuilderConfig).
            newGraphDatabase();

    setDatabase(newGraphDatabase);
    registerShutdownHook(newGraphDatabase);
  }

  public void setDatabase(GraphDatabaseService graphDB)
  {
    this.graphDB = graphDB;
    Reco4jEventHandler evtHandler = new Reco4jEventHandler();
    this.graphDB.registerTransactionEventHandler(evtHandler);
    GraphTraversal.getInstance().init(graphDB);
  }

  @Override
  public List<INode> getNodesByType(String type)
  {
    return GraphTraversal.getInstance().getNodesByType(type, neo4JGraphConfig.getNodeTypeName());
  }

  @Override
  public List<INode> getNodesByInEdge(IEdgeType edgesType)
  {
    return GraphTraversal.getInstance().getNodesByInEdge(edgesType);
  }

  private static void registerShutdownHook(final GraphDatabaseService graphDb)
  {
    Runtime.getRuntime().addShutdownHook(new Thread()
    {
      @Override
      public void run()
      {
        System.out.println("Shuting down neo4j db instance ...");
        graphDb.shutdown();
        System.out.println("... done!");
      }
    });
  }

  public GraphDatabaseService getGraphDB()
  {
    return graphDB;
  }

  @Override
  public void addEdge(INode x, INode y, IEdgeType similarityEdgeType, String propertyName, String value)
  {
    GraphTraversal.getInstance().addEdge(x, y, similarityEdgeType, propertyName, value);
  }

  @Override
  public void setEdgeProperty(IEdge edge, String propertyName, String value)
  {
    GraphTraversal.getInstance().setEdgeProperty(edge, propertyName, value);
  }

  @Override
  public List<IEdge> getEdgesByType(IEdgeType edgesType)
  {
    return GraphTraversal.getInstance().getEdgesByType(edgesType);
  }

  @Override
  public ConcurrentHashMap<Long, INode> getNodesMapByType(String type)
  {
    return GraphTraversal.getInstance().getNodesMapByType(type, neo4JGraphConfig.getNodeTypeName());
  }

  public void optimizeGraph()
  {
    //GraphTraversal.getInstance().optimizeGraph();
  }

  public void tidyUpGraph()
  {
    //This function should tidy up the graph, deleting all added relation
  }

  @Override
  public void getNodesByType(String type, IGraphCallable<INode> callback)
  {
    GraphTraversal.getInstance().getNodesByType(type, neo4JGraphConfig.getNodeTypeName(), callback);
  }

  public INode getUserNodeById(long id)
  {
    return getNodeByIdAndVerifyType(id, neo4JGraphConfig.getUserType());
  }

  public INode getItemNodeById(long id)
  {
    return getNodeByIdAndVerifyType(id, neo4JGraphConfig.getItemType());
  }

  public int getNodesNumberByType(String type)
  {
    return GraphTraversal.getInstance().getNodesNumberByType(type, neo4JGraphConfig.getNodeTypeName());
  }

  @Override
  public FastIDSet getEdgesIdByType(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getEdgesIdByType(edgeType);
  }

  @Override
  public FastIDSet getNodesIdByType(String type)
  {
    return GraphTraversal.getInstance().getNodesIdByType(type, neo4JGraphConfig.getNodeTypeName());
  }

  @Override
  public INode getNodeById(long id)
  {
    if (id < 1)
      return null;
    Node node = GraphTraversal.getInstance().getBaseNodeById(id);
    Neo4JNode inode = new Neo4JNode(node);
    return inode;
  }

  private INode getNodeByIdAndVerifyType(long id, String type)
  {
    INode inode = getNodeById(id);
    if (!inode.getProperty(neo4JGraphConfig.getNodeTypeName()).
            equalsIgnoreCase(type))
    {
      logger.log(Level.WARNING, "Requested a node for type " + type + " but it is of different type. ID: {0}", id);
      return null;
    }
    return inode;
  }

  public INode addNode(Map<String, String> properties)
  {
    return GraphTraversal.getInstance().addNode(properties);
  }

  public void setNodeProperty(INode node, String propertyName, String value)
  {
    GraphTraversal.getInstance().setNodeProperty(node, propertyName, value);
  }

  public ConcurrentHashMap<Long, INode> getNodesByQuery(String query)
  {
    return GraphTraversal.getInstance().getNodesByQuery(query);
  }
  
  public List<IEdge> getEdgesByQuery(String query)
  {
    return GraphTraversal.getInstance().getEdgesByQuery(query);
  }

  public FastIDSet getNodesIdByQuery(String query)
  {
    return GraphTraversal.getInstance().getNodesIdByQuery(query);
  }
}
