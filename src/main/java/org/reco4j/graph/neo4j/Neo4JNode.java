/*
 * Neo4JNode.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.mahout.cf.taste.impl.common.FastIDSet;
import org.neo4j.graphdb.Node;
import org.reco4j.graph.BasicNode;
import org.reco4j.graph.IEdge;
import org.reco4j.graph.IEdgeType;
import org.reco4j.graph.IGraphCallable;
import org.reco4j.graph.INode;
import org.reco4j.graph.neo4j.traversal.GraphTraversal;


/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JNode extends BasicNode
{
  private long id;
  private SoftReference<Node> nodeRef;
  
  public Neo4JNode(long id)
  {
    setNode(GraphTraversal.getInstance().getBaseNodeById(id));
  }
  
  public Neo4JNode(Node node)
  {
    setNode(node);
  }

  public final void setNode(Node node)
  {
    id = node.getId();
    this.nodeRef = new SoftReference<Node>(node);
  }

  public Node getNode()
  {
    Node node = nodeRef.get();
    if (node == null)
    {
      node = GraphTraversal.getInstance().getBaseNodeById(id);
      setNode(node);
    }
    return node;
  }

  @Override
  public void setProperty(String name, String value)
  {
    GraphTraversal.getInstance().setNodeProperty(this, name, value);
  }

  @Override
  public String getProperty(String name)
  {
    Object value = getNode().getProperty(name);
    if (value instanceof String)
      return (String) value;
    else if (value instanceof Integer)
      return ((Integer) value).toString();
    else
    {
      if (value == null)
      {
        StringBuilder exception = new StringBuilder();
        exception.append("getProperty conversion data not supported! Not available property '").append(name).append("'on node!\n");
        exception.append("Available properties are:\n");
        for (String key : getNode().getPropertyKeys())
          exception.append(key).append("\n");
        throw new RuntimeException(exception.toString());
      }
      else
      {
        throw new RuntimeException("getProperty conversion data not supported! Not supported data type for property '" + name + "': " + value.getClass());
      }

    }
  }

  @Override
  public Boolean isConnected(INode b, IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().isConnected(this, b, edgeType);
  }

  @Override
  public Boolean isConnected(INode node, List<IEdgeType> edgeTypes)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Boolean isConnectedIn(INode node, List<IEdgeType> edgeTypes)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Boolean isConnectedOut(INode node, List<IEdgeType> edgeTypes)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public IEdge getEdge(INode node, IEdgeType edgeType)
  {
    IEdge edge = GraphTraversal.getInstance().getEdge(this, node, edgeType);
    return edge;
  }

  @Override
  public List<IEdge> getInEdge(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getInEdge(this, edgeType);
  }

  @Override
  public List<IEdge> getOutEdge(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getOutEdge(this, edgeType);
  }

  @Override
  public ConcurrentHashMap<Long, INode> getCommonNodes(IEdgeType edgeType, String identifier)
  {
    return GraphTraversal.getInstance().getCommonNodes(this, edgeType, identifier);
  }
  
  @Override
  public FastIDSet getCommonNodeIds(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getCommonNodeIds(this, edgeType);
  }
  
  public void iterateOnCommonNodes(IEdgeType edgeType, IGraphCallable<INode> callback)
  {
    GraphTraversal.getInstance().getCommonNodes(this, edgeType, callback);
  }
  
  @Override
  public int getInEdgeNumber(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getInEdgeNumber(this, edgeType);
  }

  public void iterateOnEdge(IEdgeType edgeType, IGraphCallable<IEdge> callback)
  {
    GraphTraversal.getInstance().iterateOnEdge(this, edgeType, callback);
  }
  public long getId()
  {
    return id;
  }

  public FastIDSet getInEdgeIds(IEdgeType edgeType)
  {
    return GraphTraversal.getInstance().getInEdgeIds(this, edgeType);
  }
  
  public void setId(long id)
  {
    this.id = id;
  }

  public void addOutEdge(IEdgeType edgeType, INode destination)
  {
    GraphTraversal.getInstance().addEdge(this, destination, edgeType);
  }

  public void addInEdge(IEdgeType edgeType, INode source)
  {
    source.addOutEdge(edgeType, this);
  }

  public void addOutEdgeWithProperty(IEdgeType edgeType, INode destination, String propertyName, Object value)
  {
    GraphTraversal.getInstance().addEdge(this, destination, edgeType, propertyName, value);
  }

  public void addInEdgeWithProperty(IEdgeType edgeType, INode source, String propertyName, Object value)
  {
    source.addOutEdgeWithProperty(edgeType, this, propertyName, value);
  }
}
