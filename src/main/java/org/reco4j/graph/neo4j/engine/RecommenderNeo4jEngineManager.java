/*
 * RecommenderNeo4jEngineManager.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.engine;

import org.reco4j.model.Rating;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.Configuration;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.reco4j.dataset.FilteredUserItemDataset;
import org.reco4j.graph.*;
import org.reco4j.engine.RecommenderBuilderThread;
import org.reco4j.engine.RecommenderEngine;
import org.reco4j.graph.neo4j.Neo4JEdge;
import org.reco4j.graph.neo4j.Neo4JNode;
import org.reco4j.graph.neo4j.Neo4jGraph;
import org.reco4j.graph.neo4j.filter.Neo4JPostFilter;
import org.reco4j.graph.neo4j.filter.Neo4JPreFilter;
import org.reco4j.graph.neo4j.util.Neo4JPropertiesHandle;
import org.reco4j.model.IModel;
import org.reco4j.recommender.IRecommender;
import org.reco4j.recommender.knn.IKNNModel;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class RecommenderNeo4jEngineManager
{

  private static final Logger logger = Logger.getLogger(RecommenderNeo4jEngineManager.class.getName());
  
  private HashMap<String, IRecommender> recommenders = new HashMap<String, IRecommender>();
  private List<RecommenderBuilderThread> recommenderThreads = new ArrayList<RecommenderBuilderThread>();

  private RecommenderNeo4jEngineManager()
  {
  }

  public static RecommenderNeo4jEngineManager getInstance()
  {
    return RecommenderNeo4jEngeManagerHolder.INSTANCE;
  }

  private Properties loadProperties(String propertiesPath)
  {
    Properties properties = new Properties();
    try
    {
      properties.load(new FileReader(propertiesPath));
    }
    catch (IOException ex)
    {
      Logger.getLogger(RecommenderNeo4jEngineManager.class.getName()).log(Level.SEVERE, "Error while loading properties: " + propertiesPath, ex);
    }
    return properties;
  }

  public Iterable<Node> getRecommender(INode user, String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    List<Rating> recommendations = recommender.recommend(user);
    ArrayList<Node> result = new ArrayList<Node>();
    for (Rating reco : recommendations)
      result.add(((Neo4JNode) reco.getItem()).getNode());
    logger.log(Level.INFO, "Size {0}", result.size());
    return result;
  }

  public void updateRecommender(Relationship edge, int operation)
  {
    for (IRecommender recommender : recommenders.values())
      if (edge.isType(DynamicRelationshipType.withName(EdgeTypeFactory.getEdgeType(IEdgeType.EDGE_TYPE_RANK, recommender.getConfig().getGraphConfig()).getEdgeName())))
        recommender.updateRecommender(new Neo4JEdge(edge), operation);
  }

  private IRecommender getRecommenderByName(String recommenderName) throws RuntimeException
  {
    IRecommender recommender = recommenders.get(recommenderName);
    if (recommender == null)
      throw new RuntimeException("Recommender with name " + recommenderName + " does not exist!");
    return recommender;
  }

  private IRecommender initializeRecommender(Neo4JPropertiesHandle propertiesHandle, Neo4jGraph learningDataSet)
  {
    if (!propertiesHandle.isFiltered())
      return initializeBasicRecommender(propertiesHandle, learningDataSet);
    else
    {
      logger.warning("Is filtered!");
      return initializeFilteredRecommender(propertiesHandle, learningDataSet);
    }
  }
  private IRecommender initializeBasicRecommender(Neo4JPropertiesHandle propertiesHandle, Neo4jGraph learningDataSet)
  {
    IRecommender recommender = RecommenderEngine.createRecommender(propertiesHandle);
    recommender.setModelName(propertiesHandle.getModelName());
    RecommenderBuilderThread recommenderThread = new RecommenderBuilderThread(recommender, learningDataSet);
    recommenderThreads.add(recommenderThread);
    return recommender;
  }
  
  private IRecommender initializeFilteredRecommender(Neo4JPropertiesHandle propertiesHandle, Neo4jGraph learningDataSet)
  {
    IRecommender recommender = RecommenderEngine.createRecommender(propertiesHandle);
    recommender.setModelName(propertiesHandle.getModelName());
    boolean preFilterConfigured = true;
    boolean postFilterConfigured = true;
    logger.log(Level.WARNING, "propertiesHandle.getItemsQuery(): {0}", propertiesHandle.getItemsQuery());
    logger.log(Level.WARNING, "propertiesHandle.getUsersQuery(): {0}", propertiesHandle.getUsersQuery());
    logger.log(Level.WARNING, "propertiesHandle.getRatingsQuery(): {0}", propertiesHandle.getRatingsQuery());
    logger.log(Level.WARNING, "propertiesHandle.getCommonsNodeQuery(): {0}", propertiesHandle.getCommonsNodeQuery());
    if (propertiesHandle.getItemsQuery() == null || 
        propertiesHandle.getUsersQuery() == null || 
        propertiesHandle.getRatingsQuery() == null ||
        propertiesHandle.getCommonsNodeQuery() == null )
      preFilterConfigured = false;
    
    if (propertiesHandle.getPostItemsQuery() == null)
      postFilterConfigured = false;
    
    if (!preFilterConfigured && !postFilterConfigured)
      throw new RuntimeException("propertiesHandle.isFiltered() is true but there is no (complete) specification");
        
    Neo4JPreFilter preFilter = null;
    if (preFilterConfigured)                 
    {
      preFilter = new Neo4JPreFilter();
      preFilter.setItemsQuery(propertiesHandle.getItemsQuery());
      preFilter.setUsersQuery(propertiesHandle.getUsersQuery());
      preFilter.setRatingsQuery(propertiesHandle.getRatingsQuery());
      preFilter.setCommonsNodeQuery(propertiesHandle.getCommonsNodeQuery());
    }
    /*
    * Create and configure postfilter
    */
    Neo4JPostFilter postFilter = null;
    if (postFilterConfigured)
    {
      postFilter = new Neo4JPostFilter();
      postFilter.setItemsQuery(propertiesHandle.getPostItemsQuery()); 
    }
    /*
     * Create and initialize the userItem data set
     */
    logger.log(Level.WARNING, "->>>> {0} {1}", new Object[]{preFilter, postFilter});
    FilteredUserItemDataset userItemDataset = new FilteredUserItemDataset();
    userItemDataset.init(learningDataSet, propertiesHandle, preFilter, postFilter);
    
    
    RecommenderBuilderThread recommenderThread = new RecommenderBuilderThread(recommender, learningDataSet, userItemDataset);
    recommenderThreads.add(recommenderThread);
    return recommender;
  }

  private static class RecommenderNeo4jEngeManagerHolder
  {

    private static final RecommenderNeo4jEngineManager INSTANCE = new RecommenderNeo4jEngineManager();
  }

  public void init(GraphDatabaseService gds, Configuration c)
  {
    String[] propertiesPaths = c.getStringArray("org.reco4j.graph.neo4j.engine.properties");

    for (String propertiesPath : propertiesPaths)
    {
      Properties properties = loadProperties(propertiesPath);
      Neo4JPropertiesHandle propertiesHandle = new Neo4JPropertiesHandle(properties);
      
      String recommenderName = propertiesHandle.getRecommenderName();
      logger.log(Level.INFO, "Initializing {0} reco4j recommender", recommenderName);
      
      if (recommenders.containsKey(recommenderName))
      {
        logger.log(Level.SEVERE, "A recommender with name {0} already exist the second one is ignored", recommenderName);
        continue;
      }
      
      Neo4jGraph learningDataSet = new Neo4jGraph(propertiesHandle);
      learningDataSet.setDatabase(gds);
      IRecommender recommender = initializeRecommender(propertiesHandle, learningDataSet);
      
      recommenders.put(recommenderName, recommender);
      
      logger.log(Level.INFO, "{0} reco4j recommender initialized!", recommenderName);
    }
  }

  public void startRecommender()
  {
    try
    {
      for (RecommenderBuilderThread recommenderThread : recommenderThreads)
        recommenderThread.start();
    }
    catch (Exception ex)
    {
      logger.log(Level.SEVERE, "Error in recommender Thread!!!", ex);
    }
  }

  public Iterable<Node> getUsers(String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    ConcurrentHashMap<Long, INode> users = recommender.getUserItemDataset().getUserList();
    ArrayList<Node> result = new ArrayList<Node>();
    for (INode node : users.values())
      result.add(((Neo4JNode) node).getNode());
    return result;
  }

  public Iterable<Node> getItems(String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    ConcurrentHashMap<Long, INode> items = recommender.getUserItemDataset().getItemList();
    ArrayList<Node> result = new ArrayList<Node>();
    for (INode node : items.values())
      result.add(((Neo4JNode) node).getNode());
    return result;
  }

  public Iterable<Relationship> getAllRatings(String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    List<IEdge> ratings = recommender.getUserItemDataset().getRatingList();

    ArrayList<Relationship> result = new ArrayList<Relationship>();
    for (IEdge relationship : ratings)
      result.add(((Neo4JEdge) relationship).getEdge());
    return result;
  }

  public Iterable<Relationship> getRatings(Node node, String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    Neo4JNode n = new Neo4JNode(node);
    List<IEdge> ratingsByUser = recommender.getUserItemDataset().getRatingsByUser(node.getId());

    ArrayList<Relationship> result = new ArrayList<Relationship>();
    for (IEdge relationship : ratingsByUser)
      result.add(((Neo4JEdge) relationship).getEdge());
    return result;
  }

  public Iterable<Relationship> getRatingsTest(Node node, String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    Neo4JNode n = new Neo4JNode(node);
    List<IEdge> relationships = n.getOutEdge(EdgeTypeFactory.getEdgeType(IEdgeType.EDGE_TYPE_TEST_RANK, recommender.getConfig().getGraphConfig()));
    ArrayList<Relationship> result = new ArrayList<Relationship>();
    for (IEdge relationship : relationships)
      result.add(((Neo4JEdge) relationship).getEdge());
    return result;
  }

  public double getPrediction(Node node, Node item, String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);

    Neo4JNode n = new Neo4JNode(node);
    Neo4JNode i = new Neo4JNode(item);
    return recommender.estimateRating(n, i);
  }

  public Iterable<Node> getSimilarItems(Node item, int cardinality, String recommenderName)
  {
    IRecommender recommender = getRecommenderByName(recommenderName);
    IModel model = recommender.getModel();
    if (!(model instanceof IKNNModel))
      throw new RuntimeException("Model doesn't support similarity");
    Neo4JNode i = new Neo4JNode(item);
    List<Rating> similarItem = ((IKNNModel) model).getSimilarItem(i, cardinality);
    ArrayList<Node> result = new ArrayList<Node>();
    for (Rating rate : similarItem)
      result.add(((Neo4JNode) rate.getItem()).getNode());
    return result;
  }
}
