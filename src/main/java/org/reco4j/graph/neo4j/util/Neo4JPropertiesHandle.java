/*
 * Neo4JPropertiesHandle.java
 * 
 * Copyright (C) 2013 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.util;

import java.util.Properties;
import org.reco4j.util.RecommenderPropertiesHandle;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JPropertiesHandle
  extends RecommenderPropertiesHandle
  implements INeo4JGraphConfig
{
  protected final static String PROTERTY_NAME_IS_FILTERED = "isFiltered";
  protected final static String PROTERTY_NAME_PRE_FILTER_ITEMS_QUERY = "preFilterItemsQuery";
  protected final static String PROTERTY_NAME_PRE_FILTER_USERS_QUERY = "preFilterUsersQuery";
  protected final static String PROTERTY_NAME_PRE_FILTER_RATINGS_QUERY = "preFilterRatingsQuery";
  protected final static String PROTERTY_NAME_PRE_FILTER_COMMONS_NODE_QUERY = "preFilterCommonsNodeQuery";
  protected final static String PROTERTY_NAME_POST_FILTER_ITEMS_QUERY = "postFilterItemsQuery";
  
  //private static Neo4JPropertiesHandle theInstance = new Neo4JPropertiesHandle();

//  public static Neo4JPropertiesHandle getInstance()
//  {
//    return theInstance;
//  }

  public Neo4JPropertiesHandle(Properties properties)
  {
    super(properties);
  }

  @Override
  public void setProperties(Properties properties)
  {
    super.setProperties(properties);
    this.properties = properties;
    //RecommenderPropertiesHandle.getInstance().setProperties(properties);
  }

  public String getDBPath()
  {
    return properties.getProperty("dbPath", null);
  }

  public String getMovieLensBasePath()
  {
    return properties.getProperty("movieLensBasePath", null);
  }

  public String getItemsQuery()
  {
    return properties.getProperty(PROTERTY_NAME_PRE_FILTER_ITEMS_QUERY, null);
  }

  public String getUsersQuery()
  {
    return properties.getProperty(PROTERTY_NAME_PRE_FILTER_USERS_QUERY, null);
  }

  public String getRatingsQuery()
  {
    return properties.getProperty(PROTERTY_NAME_PRE_FILTER_RATINGS_QUERY, null);
  }

  public String getCommonsNodeQuery()
  {
    return properties.getProperty(PROTERTY_NAME_PRE_FILTER_COMMONS_NODE_QUERY, null);
  }

  public String getPostItemsQuery()
  {
    return properties.getProperty(PROTERTY_NAME_POST_FILTER_ITEMS_QUERY, null);
  }

  public boolean isFiltered()
  {
    return properties.getProperty(PROTERTY_NAME_IS_FILTERED, null) == null ? false : properties.getProperty(PROTERTY_NAME_IS_FILTERED, null).equalsIgnoreCase("true");
  }
}
